informer v. 0.0.1

Author: Jyri-Petteri Paloposki <jyri-petteri.paloposki@iki.fi>
License: see LICENSE file.

Description:
------------

A small Django software to ease sending messages (alerts, bulletins etc.) to predefined contact lists.
Currently supports Nexmo API, but support for any other SMS provider's API should be dead simple to do.

Note: It doesn't feature a nice UI. The current UI is made up only of Django default admin GUI. It works, but it's not
pretty and does not have a fancy access control. Pros don't need those kinds of things!

Installing:
-----------

As usual with a Django project:

# Checkout the project to a random directory (and change to the directory)

# Make a new virtualenv
$ virtualenv env

# Change to the virtualenv
$ source env/bin/activate

# Install required libraries etc.
$ pip install -r pip-req.txt

# Copy the local settings file
$ cp informer/settings/local_vanilla.py informer/settings/local.py

# And set your Nexmo API settings & perhaps your DB credentials there (no, you can't use any other editor besides VIM!)
$ vi informer/settings/local.py

Run it, using manage.py runserver or whatever you like.