from django.contrib import admin
from informer.models import Contact, List, Message

class SubscriptionInline(admin.StackedInline):
  model = List.contacts.through

  def get_extra(self, request, obj=None, **kwargs):
    if obj:
      return 0
    return 1

class ContactAdmin(admin.ModelAdmin):
  list_display = ['name', 'email', 'phone', 'lists',]
  list_filter = ['list',]
  inlines = [SubscriptionInline,]
admin.site.register(Contact, ContactAdmin)

class ListAdmin(admin.ModelAdmin):
  list_display = ['name', 'count',]
admin.site.register(List, ListAdmin)

class MessageAdmin(admin.ModelAdmin):
  list_display = ['title', 'list', 'added_on',]
admin.site.register(Message, MessageAdmin)
