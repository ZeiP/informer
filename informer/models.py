from django.db import models
from django.db.models.signals import post_save

class Contact(models.Model):
  name = models.CharField(max_length=200)
  email = models.CharField(max_length=200)
  phone = models.CharField(max_length=200)
  memo = models.TextField(blank=True)
  added_on = models.DateTimeField(auto_now_add=True)

  def lists(self):
    lists = []
    for list in List.objects.filter(contacts=self):
      lists.append(list.__unicode__())
    return ', '.join(lists)

  def __unicode__(self):
    return self.name

class List(models.Model):
  name = models.CharField(max_length=200)
  contacts = models.ManyToManyField('Contact')

  def count(self):
    return self.contacts.all().count()

  def __unicode__(self):
    return self.name

class Message(models.Model):
  title = models.CharField(max_length=200)
  body = models.TextField()
  list = models.ForeignKey('List')
  added_on = models.DateTimeField(auto_now_add=True)

  def __unicode__(self):
    return self.title + ' (' + str(self.list) + ')'

def send_message(sender, instance, created, **kwargs):
  if created:
    from nexmo import send_message
    for contact in instance.list.contacts.all():
      send_message(contact.phone, instance.body)

post_save.connect(send_message, sender=Message)
