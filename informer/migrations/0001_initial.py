# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Contact'
        db.create_table(u'informer_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'informer', ['Contact'])

        # Adding model 'List'
        db.create_table(u'informer_list', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'informer', ['List'])

        # Adding M2M table for field contacts on 'List'
        m2m_table_name = db.shorten_name(u'informer_list_contacts')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('list', models.ForeignKey(orm[u'informer.list'], null=False)),
            ('contact', models.ForeignKey(orm[u'informer.contact'], null=False))
        ))
        db.create_unique(m2m_table_name, ['list_id', 'contact_id'])

        # Adding model 'Message'
        db.create_table(u'informer_message', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('list', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['informer.List'])),
        ))
        db.send_create_signal(u'informer', ['Message'])


    def backwards(self, orm):
        # Deleting model 'Contact'
        db.delete_table(u'informer_contact')

        # Deleting model 'List'
        db.delete_table(u'informer_list')

        # Removing M2M table for field contacts on 'List'
        db.delete_table(db.shorten_name(u'informer_list_contacts'))

        # Deleting model 'Message'
        db.delete_table(u'informer_message')


    models = {
        u'informer.contact': {
            'Meta': {'object_name': 'Contact'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'informer.list': {
            'Meta': {'object_name': 'List'},
            'contacts': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['informer.Contact']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'informer.message': {
            'Meta': {'object_name': 'Message'},
            'body': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['informer.List']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['informer']